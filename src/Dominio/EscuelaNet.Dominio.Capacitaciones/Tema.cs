﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Tema : Entity
    {
        public string Nombre { get; set; }
        public NivelTema Nivel { get; set; }
        private Tema()
        {

        }

        public Tema(string nombre, NivelTema nivel) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Nivel = nivel;
        }
    }
}