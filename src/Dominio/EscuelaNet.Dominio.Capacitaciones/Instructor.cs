﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Instructor : Persona
    {
        public IList<Tema> Temas { get; set; }

        private Instructor()
        {

        }

        public Instructor(string nombre, string apellido, string dni, DateTime fechaNacimiento, List<Tema> temas) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Dni = dni ?? throw new System.ArgumentNullException(nameof(nombre));
            this.FechaNacimiento = fechaNacimiento;
            this.Temas = new List<Tema>();

            foreach (var tema in temas)
            {
                this.Temas.Add(tema);
            }
        }
    }
}
