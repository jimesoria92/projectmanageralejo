﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class ConocimientoPorPrecio
    {
        public DateTime Fecha { set; get;}
        public Nivel Nivel { set; get;}
        public string Moneda { get; set; }
        public Double ValorNominal { get; set; }

     
        public ConocimientoPorPrecio(Double valorNominal, string moneda, Nivel nivel)
        {
            if (string.IsNullOrEmpty(moneda))
            {
                throw new ArgumentException("message", nameof(moneda));
            }
            if (valorNominal <= 0)
            {
                throw new ExcepcionDeConocimiento("Un precio debe ser mayor que 0");
            }
            this.ValorNominal = ValorNominal;
            this.Moneda = moneda;
            this.Nivel = nivel;
        }
        public ConocimientoPorPrecio()
        {

        }
        
    }
}