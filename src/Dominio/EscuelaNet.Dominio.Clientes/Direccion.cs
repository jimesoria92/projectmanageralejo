﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Clientes
{
    public class Direccion : Entity
    {
        public string Domicilio { get; set; }

        public string Localidad { get; set; }

        public string Provincia { get; set; }

        public string Pais { get; set; }

        private Direccion()  {  }

        public Direccion(string domicilio, string localidad, string provincia, string pais)
        {
            this.Domicilio = domicilio ?? throw new System.ArgumentNullException(nameof(domicilio)); 
            this.Localidad = localidad ?? throw new System.ArgumentNullException(nameof(localidad));
            this.Provincia = provincia ?? throw new System.ArgumentNullException(nameof(provincia));
            this.Pais = pais ?? throw new System.ArgumentNullException(nameof(pais));
        }

        public string toString()
        {
            return this.Domicilio + ", " + this.Localidad + ", " + this.Provincia + ", " + this.Pais; 
        }
    }
}
