﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class TemaIndexModel
    {

        public string Titulo { get; set; }
        public List<Tema> Temas { get; set; }
    }
}