﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura
{
    public sealed class ContextoInstructor
    {
        private static ContextoInstructor _instancia = new ContextoInstructor();

        public List<Instructor> Instructores { get; set; }

        private ContextoInstructor()
        {
            this.Instructores = new List<Instructor>();

            var temas1 = new List<Tema>();

            temas1.Add(new Tema("MVC",NivelTema.Intermedio));
            temas1.Add(new Tema(".Net Core", NivelTema.Avanzado));

            this.Instructores.Add(new Instructor("Matias", "Apellido", "1234567", DateTime.Today, temas1));

            var temas2 = new List<Tema>();

            temas2.Add(new Tema("C++", NivelTema.Avanzado));

            this.Instructores.Add(new Instructor("Juancito", "Pepo", "123123", DateTime.Today,temas2));

            var temas3 = new List<Tema>();

            temas3.Add(new Tema("Java",NivelTema.Avanzado));

            this.Instructores.Add(new Instructor("Ramoncito", "Segundo", "1234567", DateTime.Today,temas3));

        }

        public static ContextoInstructor Instancia
        {
            get
            {
                return _instancia;
            }
        }
    }
}