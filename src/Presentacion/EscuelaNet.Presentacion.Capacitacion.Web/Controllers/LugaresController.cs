﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class LugaresController : Controller
    {
        // GET: Lugares
        public ActionResult Index()
        {
            var lugares = ContextoLugar.Instancia.Lugares;
            var model = new LugarIndexModel()
            {
                Titulo = "Mi Primera Prueba",
                Lugares = lugares
            };

            return View(model);
        }

        // GET: Lugares/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Lugares/Create
        public ActionResult New()
        {
            var model = new NuevoLugarModel();
            return View(model);
        }

        // POST: Lugares/Create
        [HttpPost]
        public ActionResult New(NuevoLugarModel model)
        {
            try
            {
                ContextoLugar.Instancia.Lugares.Add(new Lugar(model.Capacidad,model.Calle,model.Numero,model.Depto,
                model.Piso,model.Localidad,model.Provincia,model.Pais));

                TempData["success"] = "Lugar Creado";
                return RedirectToAction("Index");
            }
            catch
            {
                TempData["error"] = "error";
                return View(model);
            }
        }

        // GET: Lugares/Edit/5
        public ActionResult Edit(int id)
        {
            var lugar = ContextoLugar.Instancia.Lugares[id];
            var model = new NuevoLugarModel()
            {
                Capacidad = lugar.Capacidad,
                Id = id,
                Calle = lugar.Calle,
                Numero = lugar.Numero,
                Depto = lugar.Depto,
                Piso = lugar.Piso,
                Localidad = lugar.Localidad,
                Provincia = lugar.Provincia,
                Pais = lugar.Pais,
            };
            return View(model);
        }

        // POST: Lugares/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoLugarModel model)
        {
            try
            {
                ContextoLugar.Instancia.Lugares[model.Id].Capacidad
                    = model.Capacidad;
                ContextoLugar.Instancia.Lugares[model.Id].Calle
                    = model.Calle;
                ContextoLugar.Instancia.Lugares[model.Id].Numero
                    = model.Numero;
                ContextoLugar.Instancia.Lugares[model.Id].Depto
                    = model.Depto;
                ContextoLugar.Instancia.Lugares[model.Id].Piso
                    = model.Piso;
                ContextoLugar.Instancia.Lugares[model.Id].Localidad
                    = model.Localidad;
                ContextoLugar.Instancia.Lugares[model.Id].Provincia
                    = model.Provincia;
                ContextoLugar.Instancia.Lugares[model.Id].Pais
                    = model.Pais;


                TempData["success"] = "Lugar Editado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        // GET: Lugares/Delete/5
        public ActionResult Delete(int id)
        {
            var lugar = ContextoLugar.Instancia.Lugares[id];
            var model = new NuevoLugarModel()
            {
                Capacidad = lugar.Capacidad,
                Id = id,
                Calle = lugar.Calle,
                Numero = lugar.Numero,
                Depto = lugar.Depto,
                Piso = lugar.Piso,
                Localidad = lugar.Localidad,
                Provincia = lugar.Provincia,
                Pais = lugar.Pais,
            };
            return View(model);
        }

        // POST: Lugares/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoLugarModel model)
        {
            try
            {
                ContextoLugar.Instancia.Lugares.RemoveAt(model.Id);
                TempData["success"] = "Lugar borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
