﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class UnidadesController : Controller
    {
        
        public ActionResult Index(int id)
        {
            var cliente = Contexto.Instancia.Clientes[id];

            if (cliente.Unidades==null)
            {
                TempData["error"] = "Cliente sin unidades";
                return RedirectToAction("../Clientes/Index");
            }

            var unidades = cliente.Unidades;

            List<UnidadDeNegocio> unids = new List<UnidadDeNegocio>();

            foreach (var unidad in unidades)
            {
                unids.Add(unidad);
            }

            var model = new UnidadesIndexModel()
            {
                Titulo = "Unidades del Cliente '" + cliente.RazonSocial+"'",
                Unidades = unids,
                idCliente = id
            };

            return View(model);
        }

        public ActionResult New(int id)
        {
            var cliente = Contexto.Instancia.Clientes[id];
            var model = new NuevaUnidadModel()
            {
                Titulo = "Nueva unidad para el Cliente '"+cliente.RazonSocial+"'",
                IdC = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    var direccion = new Direccion("domicilio", "localidad", "provincia", "pais");
                    var rs = model.RazonSocial;
                    var resp = model.ResponsableDeUnidad;
                    var cuit = model.Cuit;
                    var email = model.EmailResponsable;
                    var tel = model.TelefonoResponsable;

                    var unidad = new UnidadDeNegocio(rs,resp,cuit,email,tel,direccion);
                    var idcli = model.IdC;

                    Contexto.Instancia.Clientes[model.IdC].AgregarUnidad(unidad);
                    TempData["success"] = "Unidad creada";
                    return RedirectToAction("Index/"+model.IdC);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Edit(int idCliente, int idUnidad)
        {
            var cantClientes = Contexto.Instancia.Clientes.Count();

            if (idCliente < 0 || idCliente >= cantClientes || idUnidad <0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var cliente = Contexto.Instancia.Clientes[idCliente];
                var unidad = cliente.Unidades[idUnidad];

                var model = new NuevaUnidadModel()
                {
                    Titulo="Editar la Unidad '"+unidad.RazonSocial+"' del Cliente '"+cliente.RazonSocial+"'",
                    IdC = idCliente,
                    IdU = idUnidad,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                                       
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaUnidadModel model)
        {
            if (!string.IsNullOrEmpty(model.RazonSocial))
            {
                try
                {
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].RazonSocial = model.RazonSocial;
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].ResponsableDeUnidad = model.ResponsableDeUnidad;
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].Cuit = model.Cuit;
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].EmailResponsable = model.EmailResponsable;
                    Contexto.Instancia.Clientes[model.IdC].Unidades[model.IdU].TelefonoResponsable = model.TelefonoResponsable;

                    TempData["success"] = "Unidad editada";
                    return RedirectToAction("Index/"+model.IdC);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int idCliente, int idUnidad)
        {
            var cantidad = Contexto.Instancia.Clientes.Count();
            if (idCliente < 0 || idCliente >= cantidad || idUnidad <0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var cliente = Contexto.Instancia.Clientes[idCliente];
                var unidad = cliente.Unidades[idUnidad];

                var model = new NuevaUnidadModel()
                {
                    Titulo = "Borrar la Unidad '" + unidad.RazonSocial + "' del Cliente '" + cliente.RazonSocial + "'",
                    IdC = idCliente,
                    IdU = idUnidad,
                    RazonSocial = unidad.RazonSocial,
                    ResponsableDeUnidad = unidad.ResponsableDeUnidad,
                    EmailResponsable = unidad.EmailResponsable,
                    Cuit = unidad.Cuit,
                    TelefonoResponsable = unidad.TelefonoResponsable,
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaUnidadModel model)
        {

            try
            {
                Contexto.Instancia.Clientes[model.IdC].Unidades.RemoveAt(model.IdU);
                TempData["success"] = "Unidad borrada";
                return RedirectToAction("../Unidades/Index/"+model.IdC);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}